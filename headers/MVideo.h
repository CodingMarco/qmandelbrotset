#pragma once
#include "MImage.h"
#include "Mandelbrotset.h"
#include "mbnamespace.h"

class MVideo
{
public:
	MVideo(unsigned int m_fps, unsigned int m_length);
	void compute();

	// Getter
	unsigned int getLength() const { return length; }
	unsigned int getFps() const { return fps; }

	// Setter
	void setLength(unsigned int m_length);
	void setFps(unsigned int m_fps);

private:
	unsigned int fps;
	unsigned int length;
	unsigned int images;
	MImage* image;
	MandelbrotSet* mbset;
};
