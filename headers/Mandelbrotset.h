#pragma once

#include "../headers/MImage.h"
#include "../headers/mbnamespace.h"
#include <complex>
#include <iostream>
#include <vector>

class MandelbrotSet
{
public:
	MandelbrotSet();
	void attachToImage(MImage* m_image);
	void compute();

	// Setter
	void setCenter(std::complex<double> m_center);
	void setRadius(double m_radius);
	void setMaxIterations(unsigned int m_max_iterations);
	void setExponent(double m_exponent);
	void setMaxMagnitude(double m_max_magnitude);
	void setColorMode(Mb::ColorMode m_color_mode);

	// Getter

	std::complex<double> getCenter() const { return center; }
	double getRadius() const { return radius; }
	unsigned int getMaxIterations() const { return max_iterations; }
	double getExponent() const { return exponent; }
	double getMaxMagnitude() const { return max_magnitude; }

private:
	MImage* image = nullptr;
	std::complex<double> center = {0, 0};
	double radius = 1.2;
	unsigned int max_iterations = 50;
	double exponent = 2;
	double max_magnitude = 2;
	std::vector<double>* hCoordinates; // imaginary parts
	std::vector<double>* wCoordinates; // real parts
	Mb::ColorMode color_mode = Mb::ColorList;
	Mb::rgb_color intToColor(int n);
};
