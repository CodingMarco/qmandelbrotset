#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <filesystem>
#include "mbnamespace.h"

class MImage
{
public:
	MImage(unsigned int m_width, unsigned int m_height);
	~MImage();

	// Setter for properties
	void setSize(unsigned int m_width, unsigned int m_height);
	void setFilenamePrefix(std::string m_filename_prefix);
	void setConvertedFiletype(std::string m_converted_filetype);
	void setCountingMode(Mb::CountingMode m_counting_mode);
	void setDirectory(std::filesystem::path dir);

	// Getter
	unsigned int getWidth() { return width; }
	unsigned int getHeight() { return height; }
	std::string getFilenamePrefix() { return filename_prefix; }
	std::string getConvertedFiletype() { return converted_filetype; }
	Mb::CountingMode getCountingMode() { return counting_mode; }
	std::filesystem::path getDirectory() { return directory; }

	void setPixel(unsigned int m_width, unsigned int m_height, Mb::rgb_color m_color);


private:
	unsigned int width = 0, height = 0;
	std::vector<Mb::rgb_color>* pixels;
	std::string filename_prefix = "mb";
	std::string converted_filetype = "png";
	std::filesystem::path directory = ".";
	std::ofstream file;
	Mb::CountingMode counting_mode = Mb::Increment;
	int current_image = 0;
	void writeToFile();
	template<class T> void writeToFile(T CustomIndex);

	friend class MandelbrotSet;
};
