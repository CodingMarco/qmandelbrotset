#include <iostream>
#include <vector>
#include <fstream>
#include <complex>
#include <thread>
#include <chrono>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <random>

#include "../headers/Mandelbrotset.h"

struct rgb_color
{
	uint8_t r, g, b;
	rgb_color(uint8_t m_r, uint8_t m_g, uint8_t m_b)
	{
		r = m_r;
		g = m_g;
		b = m_b;
	}
};

void createImages_exp(unsigned int length, unsigned int fps)
{
	MImage image(1920, 1080);
	image.setFilenamePrefix("mandelb");
	image.setCountingMode(Mb::Custom);
	image.setConvertedFiletype("png");
	image.setDirectory(".");

	MandelbrotSet set;
	set.setColorMode(Mb::ColorList);
	set.attachToImage(&image);

	unsigned int images = length * fps;

	std::cout << images << " images will be created..." << std::endl;

	for(int x = 0; x < images; x++)
	{
		double tmp = -expl(-0.1*x+log(2));
		double exponent = tmp+2;
		std::cout << exponent << std::endl;
		set.setExponent(exponent);
		set.compute();
	}

//	for(double exponent = e_min; exponent < e_max; exponent += (e_max - e_min) / images)
//	{
//		//mandelbrot2image(1920, 1080, std::complex<double>(-2,0), radius, 60, exponent);
//	}
	//mandelbrot2image(1920, 1080, std::complex<double>(-2,0), radius, 60, e_max);
}

int main()
{
	createImages_exp(10, 15);
    return 0;
}
