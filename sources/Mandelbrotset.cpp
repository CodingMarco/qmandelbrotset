#include "../headers/Mandelbrotset.h"

MandelbrotSet::MandelbrotSet()
{

}

void MandelbrotSet::attachToImage(MImage *m_image)
{
	image = m_image;
}

void MandelbrotSet::compute()
{
	if(image == nullptr)
	{
		std::cout << "ERROR: No image attached!" << std::endl;
		exit(EXIT_FAILURE);
	}
	Mb::Timer t;
	std::cout << "Creating Image" << " / ";
	std::cout.flush();

	delete hCoordinates;
	delete wCoordinates;
	hCoordinates = new std::vector<double>;
	wCoordinates = new std::vector<double>;
	hCoordinates->reserve(image->getHeight());
	wCoordinates->reserve(image->getWidth());
	for(unsigned int hpos = 0; hpos < image->getHeight(); hpos++)
	{
		hCoordinates->emplace_back(-( ( center.imag() - radius ) + ( ( 2 * radius * hpos ) / image->getHeight() ) ));
	}
	for(unsigned int wpos = 0; wpos < image->getWidth(); wpos++)
	{
		wCoordinates->emplace_back(center.real() - ( radius * (double(image->getWidth()) / image->getHeight()))
								  + ( 2 * radius * (double(image->getWidth()) / image->getHeight())) * wpos / image->getWidth());
	}

	#pragma omp parallel for ordered schedule(dynamic)
	for(unsigned int hpos = 0; hpos < image->getHeight(); hpos++)
	{
		for(unsigned int wpos = 0; wpos < image->getWidth(); wpos++)
		{
			std::complex<double> z((*wCoordinates)[wpos], (*hCoordinates)[hpos]);
			std::complex<double> c = z;
			unsigned int current_iteration = 0;
			double previousMagnitude = std::abs(z);
			while(current_iteration < max_iterations && previousMagnitude < max_magnitude)
			{
				z = std::pow(z, exponent) + c;
				double newMagnitude = std::abs(z);
				if(newMagnitude == previousMagnitude)
				{
					current_iteration = max_iterations;
					break;
				}
				previousMagnitude = newMagnitude;
				current_iteration++;
			}
			switch(color_mode)
			{
				case Mb::ColorList:
					image->setPixel(wpos, hpos, current_iteration == max_iterations ? Mb::rgb_color(0,0,0) : Mb::rgb_colors[current_iteration % 5 + 1]);
					break;
				case Mb::NumberToColor:
					image->setPixel(wpos, hpos, current_iteration == max_iterations ? Mb::rgb_color(0,0,0) : intToColor(current_iteration));
					break;
			}

		}
	}

	image->writeToFile(exponent);
}

void MandelbrotSet::setCenter(std::complex<double> m_center)
{
	center = m_center;
}

void MandelbrotSet::setRadius(double m_radius)
{
	radius = m_radius;
}

void MandelbrotSet::setMaxIterations(unsigned int m_max_iterations)
{
	if(m_max_iterations > 10000)
		std::cout << "WARNING: Max. iterations seems excessive: " << m_max_iterations << std::endl;
	max_iterations = m_max_iterations;
}

void MandelbrotSet::setExponent(double m_exponent)
{
	exponent = m_exponent;
}

void MandelbrotSet::setMaxMagnitude(double m_max_magnitude)
{
	max_magnitude = m_max_magnitude;
}

void MandelbrotSet::setColorMode(Mb::ColorMode m_color_mode)
{
	color_mode = m_color_mode;
}

Mb::rgb_color MandelbrotSet::intToColor(int n)
{
	uint8_t r = n * 4 % 255;
	uint8_t g = n * 2 % 255;
	uint8_t b = n * 8 % 255;
	return Mb::rgb_color(r, g, b);
}
