#include "../headers/MVideo.h"
#include <iostream>

MVideo::MVideo(unsigned int m_fps, unsigned int m_length)
{
	if(fps != 0 && length != 0)
	{
		fps = m_fps;
		length = m_length;
		images = fps * length;
		std::cout << "INFO: " << images << " images will be created!" << std::endl;
	}
	else
	{
		std::cout << "ERROR: FPS and length must not be 0!" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void MVideo::compute()
{

}

void MVideo::setLength(unsigned int m_length)
{
	length = m_length;
	images = fps * length;
}

void MVideo::setFps(unsigned int m_fps)
{
	fps = m_fps;
	images = fps * length;
}
