#include "../headers/MImage.h"
#include <iostream>

MImage::MImage(unsigned int m_width, unsigned int m_height)
	: width(m_width), height(m_height)
{
	if(m_width == 0 || m_height == 0)
	{
		std::cout << "ERROR: Width and height must not be 0!" << std::endl;
		exit(EXIT_FAILURE);
	}
	else
		pixels = new std::vector<Mb::rgb_color>(width * height, Mb::rgb_color(0,0,0));
}

MImage::~MImage()
{
	delete pixels;
}

void MImage::setSize(unsigned int m_width, unsigned int m_height)
{
	if(m_width == 0 || m_height == 0)
	{
		std::cout << "ERROR: Width and height must not be 0!" << std::endl;
		exit(EXIT_FAILURE);
	}
	else
	{
		width = m_width;
		height = m_height;
		delete pixels;
		pixels = new std::vector<Mb::rgb_color>(width * height, Mb::rgb_color(0,0,0));
	}
}

void MImage::setFilenamePrefix(std::string m_filename_prefix)
{
	if(m_filename_prefix.size() > 0)
	{
		filename_prefix = m_filename_prefix;
	}
	else
	{
		std::cout << "ERROR: Invalid filename prefix!" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void MImage::setConvertedFiletype(std::string m_converted_filetype)
{
	if(m_converted_filetype.size() > 0)
	{
		converted_filetype = m_converted_filetype;
	}
	else
	{
		std::cout << "ERROR: Invalid converted filetype!" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void MImage::setCountingMode(Mb::CountingMode m_counting_mode)
{
	counting_mode = m_counting_mode;
}

void MImage::setDirectory(std::filesystem::path dir)
{
	if(std::filesystem::is_directory(dir))
		directory = dir;
	else
	{
		std::cout << "ERROR: Not a directory: " << dir.string() << std::endl;
		exit(EXIT_FAILURE);
	}
}

void MImage::setPixel(unsigned int m_width, unsigned int m_height, Mb::rgb_color m_color)
{
	pixels->at(m_height * width + m_width) = m_color;
}

void MImage::writeToFile()
{
	if(counting_mode == Mb::Custom)
	{std::filesystem::path directory = ".";
		std::cout << "ERROR: No custom counting value given!" << std::endl;
		exit(EXIT_FAILURE);
	}
//	if(!std::filesystem::exists(directory))
//		std::filesystem::create_directory(directory);
	std::filesystem::path full_path;
	switch(counting_mode)
	{
		case Mb::Increment:
			full_path = directory.append(filename_prefix + "_" + std::to_string(current_image) + ".ppm");
			break;
		case Mb::Random:
			full_path = directory.append(filename_prefix + "_" + std::to_string(clock() % 1000) + ".ppm");
			break;
		case Mb::Custom:
			break; // never reached
	}
	file.open(full_path, std::ios::out | std::ios::trunc);
	file << "P6\n" // P6 for the RGB values in binary
		 << width << ' ' << height
		 << "\n255\n"; // max RGB value of 255
	for(Mb::rgb_color pixel : *pixels)
	{
		file << pixel.r << pixel.g << pixel.b;
	}
	file.close();
	system((std::string("convert ") + full_path.string() + " " + full_path.replace_extension(converted_filetype).string()).c_str());
	std::filesystem::remove(full_path.replace_extension("ppm"));
}

template<class T>
void MImage::writeToFile(T CustomIndex)
{
	if(counting_mode != Mb::Custom)
	{
		std::cout << "ERROR: Custom index mode not chosen but custom index supplied." << std::endl;
		exit(EXIT_FAILURE);
	}
	if(!std::filesystem::exists(directory))
		std::filesystem::create_directory(directory);
	std::filesystem::path full_path = directory.append(filename_prefix + "_" + std::to_string(CustomIndex) + ".ppm");
	directory.remove_filename();
	file.open(full_path, std::ios::out | std::ios::trunc);
	file << "P6\n" // P6 for the RGB values in binary
		 << width << ' ' << height
		 << "\n255\n"; // max RGB value of 255
	for(Mb::rgb_color pixel : *pixels)
	{
		file << pixel.r << pixel.g << pixel.b;
	}
	file.close();
	system((std::string("convert ") + full_path.replace_extension("ppm").string() + " " + full_path.replace_extension(converted_filetype).string()).c_str());
	std::filesystem::remove(full_path.replace_extension("ppm"));
}

template void MImage::writeToFile<int>(int);
template void MImage::writeToFile<double>(double);
template void MImage::writeToFile<float>(float);
